#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    from gevent import monkey
    monkey.patch_all()

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "game.settings")
    import traceback
    from django.core.signals import got_request_exception
    def exception_printer(sender, **kwargs):
        traceback.print_exc()
    got_request_exception.connect(exception_printer)

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
