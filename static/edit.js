edit = 1;
mouse = new Object();
mouse.down = 0;
mouse.attatch = function(el) {
    el.onmousemove = mouse.getxy(mouse.pos);
    el.onmousedown = mouse.getxy(mouse.click);
    // e.button may not work in IE
    window.onmousedown =  function(e) {
        if(e.button == 0) {
            mouse.down = 1;
        }
    } 
    window.onmouseup =  function(e) {
        if(e.button == 0) {
            mouse.down = 0;
       } 
    }
}

mouse.getxy = function (f) { return function(e) {
    f(e.offsetX?e.offsetX:e.layerX, e.offsetY?e.offsetY:e.layerY);
} }
mouse.pos = function(x,y) {
    if (!picker.fullscreen) {
        if (mouse.down && x < CANVASWIDTH) {
            mouse.draw(x,y);
        }
    }
}
mouse.click = function(x,y) {
    if (picker.fullscreen == 2) {
        picker.solidpick(Math.floor(x/TILEWIDTH)*TILEWIDTH, Math.floor(y/TILEWIDTH)*TILEWIDTH);
    }
    else if (picker.fullscreen == 1) {
        picker.morepick(Math.floor(x/TILEWIDTH)*TILEWIDTH, Math.floor(y/TILEWIDTH)*TILEWIDTH);
    }
    else if (x >= CANVASWIDTH) {
        picker.pick(Math.floor(y/TILEWIDTH));
    }
    else {
        mouse.draw(x,y);
    }
}
mouse.draw = function(x,y) {
    if (picker.current) 
        world.addsquare(x,y,picker.current.pk);
    else
        world.addsquare(x,y,null);
    world.draw();
}


picker = new Object();
picker.materials = new Array();
picker.fullscreen = 0;
picker.draw = function (objects) {
    picker.materials = objects;
    for (var i=0; i<objects.length; i++) {
        world.context.fillStyle = objects[i].fields.color;
        world.context.fillRect(CANVASWIDTH, TILEWIDTH*i, TILEWIDTH, TILEWIDTH);
    }
    world.context.fillStyle = 'red';
    world.context.font = '40px/2 Unknown Font, sans-serif';
    world.context.fillText('+', CANVASWIDTH, CANVASHEIGHT);
}
picker.pick = function (i) {
    // FIXME shouldn't be hardcoded
    if (i == 14) picker.more();
    picker.current = picker.materials[i];
}
picker.remore = function () {
    picker.more(picker.re_pk, picker.re_name)
}
picker.more = function (pk, name) {
    picker.re_pk=pk; picker.re_name = name;
    picker.fullscreen = 1;
    picker.sheetpk = pk;
    var i = new Image;
    i.src = '/static/'+name;
    world.context.fillStyle = 'black';
    world.context.fillRect(0, 0, CANVASWIDTH, CANVASHEIGHT);
    world.context.drawImage(i, 0, 0);
}
picker.morepick = function(x,y) {
    f = function (material) {
        picker.current = material[0];
        world.materials[material[0].pk] = material[0].fields;
        world.draw();
    }
    jsonrequest('/api/picker?x='+x+'&y='+y+'&sheet='+picker.sheetpk, f);
}
picker.sheets = function (objects) {
    for (var i=0; i<objects.length; i++) {
        var li = document.createElement('li');
        var a = document.createElement('a');
        li.appendChild(a);
        var name = objects[i].fields.filename;
        a.href="javascript:picker.more("+objects[i].pk+", '"+name+"')";
        a.appendChild(document.createTextNode(name));
        document.getElementById('sslist').appendChild(li);
    }
}
picker.solidhash = new Array();
picker.solid = function () {
    picker.fullscreen = 2;
    world.context.fillStyle = 'black';
    world.context.fillRect(0, 0, CANVASWIDTH, CANVASHEIGHT);
    x = 0; y = 0;
    world.context.fillStyle = 'red';
    for (var pk in world.materials) {
        m = world.materials[pk];
        if (m.sheet) {
            if (world.imgs[m.sheet] == undefined) {
                world.imgs[m.sheet] = new Image;
                world.imgs[m.sheet].src = '/static/'+world.sheets[m.sheet].filename;
            }
            world.context.drawImage(world.imgs[m.sheet], m.x,m.y,m.w,m.h,x,y,m.w,m.h)
            if (m.solid) world.context.fillRect(x,y,10,10);
            picker.solidhash[x+'_'+y] = pk;
            x += m.w;
            if (x >= CANVASWIDTH) {
                y+=m.h;
                x = 0;
            }
        }
    }
}
picker.solidpick = function (x,y) {
    pk = picker.solidhash[x+'_'+y]
    jsonrequest('/api/solid?pk='+pk, function(){})
    jsonrequest('/api/material', function(r) {
        world.callback(r);
        picker.solid(); 
    });
}


you.refresh = function () {}
world.squareIsSolid = function (x,y) { return false }
world.checkstate = function() {
    if (!world.running && world.materials.length) {
        world.running = 1;
        world.draw();
    }
}
world.checkCharacters = function () {}
world.drawCharacters = function () {}
world.handlekeyExtra = function(code) {
    switch (code) {
        case 65:
            world.edit_append = 1;
            world.edit_delete = 0;
            break;
        case 88:
            world.edit_delete = 1;
            break;
        case 83:
            world.edit_append = 0;
            world.edit_delete = 0;
            break;
        case 82:
            picker.remore();
            break;
        case 87:
            picker.solid();
            break;
    }
}


function initCanvas() {
    world.init()
    mouse.attatch(world.canvas);
    jsonrequest('/api/spritesheet', picker.sheets);
    jsonrequest('/api/material', picker.draw);
}

window.onload = initCanvas;

