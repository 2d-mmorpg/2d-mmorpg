edit = 0;
TILEWIDTH = 32;
CANVASWIDTH = 640;
CANVASHEIGHT = 480;
you = new Object();
you.x = 0;
you.y = 0;
you.canvasX = 256;
you.canvasY = 192;
you.name = ""
you.characters = new Array();
you.place = function(characters) {
    you.characters = characters; 
    you.name = characters[0].fields.name
    you.x = characters[0].fields.x;
    you.y = characters[0].fields.y;
    world.checkstate();
}
you.refresh = function() {
    you.characters[0].fields.x = you.x
    you.characters[0].fields.y = you.y
    jsonrequest('/api/inform/'+you.name+'/'+world.counter, function(){}, you.characters);
    world.counter++;
}

world = new Object();
world.running = 0;
world.materials = new Array();
world.squares = new Array();
world.squaresready = 0;
world.squarehash = new Array();
world.characters = new Array();
world.charactersready = 0;
world.counter = 0;

world.sheets = new Array();
world.imgs = new Array();

world.callback = function(r) {
    if (r) {
        for (var i=0; i<r.length; i++) {
            if (r[i].model == "game.character") {
                world.characters[r[i].pk] = r[i].fields;
            } else if (r[i].model == "game.square") {
                world.squares[r[i].pk] = r[i].fields;
            } else if (r[i].model == "game.spritesheet") {
                world.sheets[r[i].pk] = r[i].fields;
            } else if (r[i].model == "game.material") {
                world.materials[r[i].pk] = r[i].fields;
            }
        }
        if (world.running) world.draw();
    }
}
world.checkUpdate = function(r) {
    world.callback(r);
    jsonrequest('/api/updates', world.checkUpdate);
}
world.init = function() {
    world.canvas = document.getElementById('canvas');
    world.context = world.canvas.getContext('2d');
    img = document.getElementById('blueman');
    img = new Image;
    img.src = '/static/base_assets/soldier.png';

    jsonrequest('/api/material', function(r) {
        world.callback(r);
        world.checkstate();
    });
    jsonrequest('/api/spritesheet', world.callback);
    jsonrequest('/api/character', world.callback);
    jsonrequest('/api/square', world.callback);
    world.checkUpdate();
} 
world.checkstate = function() {
    if (!world.running && world.materials.length && you.characters.length) {
        world.running = 1;
        world.draw();
    }
}
world.refresh = function() {
    if (world.running) {
        world.draw();
    }
}
world.draw = function() {
    if (edit) picker.fullscreen = 0;
    var c = world.context;
    c.fillStyle = 'black';
    c.fillRect(0, 0, CANVASWIDTH, CANVASHEIGHT);
    for (var pk in world.squares) {
        var square = world.squares[pk]
        if (!square) continue;
        if (!world.squarehash[square.x+"_"+square.y])
            world.squarehash[square.x+"_"+square.y] = new Array();
        world.squarehash[square.x+"_"+square.y][pk] = square;

        var x = (square.x-you.x)*TILEWIDTH + you.canvasX;
        var y = (square.y-you.y)*TILEWIDTH + you.canvasY;
        if (x<0 || y<0 || x>=CANVASWIDTH || y>=CANVASHEIGHT) continue;
        if (world.materials[square.material].sheet) {
            m = world.materials[square.material];
            if (world.imgs[m.sheet] == undefined) {
                world.imgs[m.sheet] = new Image;
                world.imgs[m.sheet].src = '/static/'+world.sheets[m.sheet].filename;
            }
            c.drawImage(world.imgs[m.sheet], m.x,m.y,m.w,m.h,x,y,m.w,m.h)
        }
        else {
            c.fillStyle = world.materials[square.material].color;
            c.fillRect(x,y, TILEWIDTH, TILEWIDTH);
        }
    }
    world.drawCharacters(c)
    c.drawImage(img,0,128, 64, 64, you.canvasX-16, you.canvasY-32, 64, 64);
}
world.drawCharacters = function(c) {
    for (var pk in world.characters) {
        if (pk==you.characters[0].pk) continue;
        var character = world.characters[pk]
        var x = (character.x-you.x)*TILEWIDTH + you.canvasX;
        var y = (character.y-you.y)*TILEWIDTH + you.canvasY;
        if (x<0 || y<0 || x>=CANVASWIDTH || y>=CANVASHEIGHT) continue;
        c.drawImage(img,0,128, 64, 196, x, y, 64, 64);
    }
}
world.squareIsSolid = function (x,y) {
    if (world.squarehash[x+"_"+y]) {
        for (var pk in world.squarehash[x+"_"+y]) {
            if (world.materials[world.squarehash[x+"_"+y][pk].material].solid)
                return true;
        }
        return false;
    } else return true;
}
world.lastsquare = "";
world.newsquarecount = -1;
world.edit_append = 0;
world.edit_delete = 0;
world.addsquare = function(x,y,material) {
    squareX = Math.floor((x-you.canvasX)/TILEWIDTH) + you.x;
    squareY = Math.floor((y-you.canvasY)/TILEWIDTH) + you.y;
    // Avoid uneccessary repitition
    if (squareX+","+squareY+","+material != world.lastsquare) {
        world.lastsquare = squareX+","+squareY+","+material;
        var newpk = null;
        for (var pk in world.squares) {
            if (!world.edit_append && world.squares[pk] && world.squares[pk].x == squareX && world.squares[pk].y == squareY)
               newpk = pk; 
        } 
        if (world.edit_delete) {
            if (newpk)
                jsonrequest("/api/delete/square/"+newpk, function (r) { });
            return;
        }
        square = {
            pk: newpk,
            model: 'game.square',
            fields: {
                x: squareX,
                y: squareY,
                material: material
            }
        };
        //if (newpk == null) {
        //    world.squares[world.newsquarecount] = square.fields;
        //    world.newsquarecount--;
        //}
        //else
        //    world.squares[newpk] = square.fields
        jsonrequest("/api/square", function(r) {

        }, [square]);
    }
}

world.handlekey = function(code) {
    if (world.running) {
        newX = you.x
        newY = you.y;
        switch (code) {
            case 37:
                newX = you.x - 1; 
                break;
            case 38:
                newY = you.y - 1; 
                break;
            case 39:
                newX = you.x + 1; 
                break;
            case 40:
                newY = you.y + 1;
                break;
            default:
                world.handlekeyExtra(code);
                return;
                break;
        }
        if (!world.squareIsSolid(newX,newY)) {
            you.x = newX;
            you.y = newY;
            you.refresh(); 
        }
        world.refresh();
    }
}
world.handlekeyExtra = function (code) {}


function jsonrequest(url, f, data) {
    var xmlhttp=new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
            f(JSON.parse(xmlhttp.responseText));
    }
    xmlhttp.open(data?'POST':'GET', url, true);
    xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xmlhttp.send(JSON.stringify(data));
}


window.onkeydown = function(e){ world.handlekey(e.keyCode?e.keyCode:e.charCode); };

function begin() {
    name = document.getElementById('name').value
    jsonrequest('/api/character/'+name, you.place)
    world.init()
    document.getElementById('canvas').style.display = 'inline';
    document.getElementById('start').style.display = 'none';
}

