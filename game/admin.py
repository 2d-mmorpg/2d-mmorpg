from django.contrib import admin
from game.models import *

admin.site.register(SpriteSheet)
admin.site.register(Material)
admin.site.register(Square)
admin.site.register(Character)
