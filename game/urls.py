from django.conf.urls import patterns, include, url
from game.views import GameView, GameEditView
from game import api

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', GameView.as_view()),
    url(r'^edit$', GameEditView.as_view()),
    url(r'^api/updates?$', "game.api.updates"),
    url(r'^api/updates?$', "game.api.updates"),
    url(r'^api/picker$', "game.api.picker"),
    url(r'^api/solid$', "game.api.solid"),
    url(r'^api/delete/(?P<model>[^/]*)/(?P<pk>[^/]*)$', "game.api.delete"),
    url(r'^api/inform/(?P<name>[^/]*)/(?P<counter>[^/]*)$', "game.api.inform"),
    url(r'^api/(?P<model>[^/]*)(/(?P<name>[^/]*))?/?$', "game.api.model_view"),
    # url(r'^game/', include('game.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
