from django.views.generic import TemplateView

class GameView(TemplateView):
    template_name="game.html"

class GameEditView(GameView):
    def get_context_data(self, **kwargs):
        context = super(GameView, self).get_context_data(**kwargs)
        context["edit"] = True
        return context
