from django.db import models

class SpriteSheet(models.Model):
    filename = models.CharField(max_length=256)

    def __str__(self): return self.filename

class Material(models.Model):
    color = models.CharField(null=True, blank=True, max_length=10)
    solid = models.BooleanField(default=False)
    sheet = models.ForeignKey(SpriteSheet, null=True)
    x = models.IntegerField(default=0)
    y = models.IntegerField(default=0)
    w = models.IntegerField(default=32)
    h = models.IntegerField(default=32)

    def __str__(self): return str(self.pk)

class Square(models.Model):
    x = models.IntegerField()
    y = models.IntegerField()
    material = models.ForeignKey(Material)

    def __str__(self): return str(self.x)+", "+str(self.y)

class Character(models.Model):
    name = models.CharField(max_length=20)
    x = models.IntegerField()
    y = models.IntegerField()


