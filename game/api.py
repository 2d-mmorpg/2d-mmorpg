from django import http
from django.core import serializers
from game.models import *
from django.views.decorators.csrf import csrf_exempt
from django.core.cache import cache
from gevent.event import Event
import gevent
import gevent.queue

# https://bitbucket.org/denis/gevent/src/d0cf5d8a4ce0/examples/webchat/chat/views.py
def json_response(stuff):
     return http.HttpResponse(stuff, content_type="application/json")

class Movement(object):

    def __init__(self):
        self.cache = {}
        self.counter = 0
        self.dbcounter = 0
        gevent.spawn(self.todb)
        # Grab values from db

    def inform(self, request, name, counter):
        response = http.HttpResponse("[]", content_type="application/json")
        if request.is_ajax and request.method == "POST":
            counter = int(counter)
            if counter == 0 or "incounter" not in request.session or counter > request.session["incounter"]: 
                request.session["incounter"] = counter
                self.cache[name] = request.body
                self.counter += 1

                changes.add(request.body)
            return response
    
    def todb(self):
        gevent.sleep(10)
        if self.counter > self.dbcounter:
            safe_db_insert(self.json())
            self.dbcounter = self.counter
        self.todb()

class Changes(object):
    def __init__(self):
        self.cache = []
        self.event = Event()
 
    def add(self, item): 
        self.cache.append(item)
        self.event.set()
        self.event.clear()

    def updates(self, request):
        if "changecounter" not in request.session: request.session["changecounter"] = 0
        counter = request.session["changecounter"]
        if len(self.cache) <= counter:
            self.event.wait()
        request.session["changecounter"] = len(self.cache)
        return json_response(self.json(self.cache[counter:]))
        
    def json(self, values):
        out = ",".join(map(lambda x: x[1:-1], values))
        return "["+out+"]"


move = Movement()
inform = csrf_exempt(move.inform)

changes = Changes()
updates = changes.updates

json_serializer = serializers.get_serializer("json")()

def safe_db_insert(body):
    for obj in serializers.deserialize("json", body):
        if obj.object._meta.app_label == "game" and obj.object._meta.object_name in ["Square","Character"]: # Needs character security
            #if obj.object._meta.object_name == "Square":
                #try: # Currently done on both sides..
                #    s = Square.objects.get(x=obj.object.x, y=obj.object.y)
                #    obj.object.pk = s.pk
                #except Square.DoesNotExist: pass
            obj.save()
            if obj.object._meta.object_name == "Square":
                jsonobj = json_serializer.serialize([obj.object], ensure_ascii=False)
                changes.add(jsonobj)

@csrf_exempt
def model_view(request, model, name=None):
    response = http.HttpResponse(content_type="application/json")
    if request.is_ajax and request.method == "POST":
        response.write("[]")
        safe_db_insert(request.body)
        return response
    queryset = None
    if model=="material": query_object = Material
    elif model=="square": query_object = Square
    elif model=="spritesheet": query_object = SpriteSheet
    elif model=="character" and name:
        queryset = Character.objects.filter(name=name)
        if not queryset:
            c = Character(name=name, x=0, y=0)
            c.save()
            queryset = Character.objects.filter(name=name)
    elif model=="character":
        query_object = Character
        #chars = cache.get("django.game.characters")
        #if not chars:
        #    chars = Character.objects.all().values("name")
        #    cache.set("django.game.characters", chars, 30)
        #queryset = chars
        #response.write("[]")
        #return response
    else: raise http.Http404
    if not queryset:
        queryset = query_object.objects.all()
    json_serializer.serialize(queryset, ensure_ascii=False, stream=response)
    return response


def delete(request, model, pk):
    if model=="square": query_object = Square
    query_object.objects.get(pk=int(pk)).delete()
    changes.add('[{"pk":'+pk+', "model": "game.square", "fields":null}]')


    response = http.HttpResponse(content_type="application/json")
    response.write("[]")
    return response



def picker(request):
    x = request.GET["x"]
    y = request.GET["y"]
    sheet = SpriteSheet.objects.get(pk=int(request.GET["sheet"])) 
    response = http.HttpResponse(content_type="application/json")
    try:
        material = Material.objects.get(x=x, y=y, sheet=sheet)
    except Material.DoesNotExist:
        material = Material(x=x, y=y, sheet=sheet, w=32, h=32)
        material.save()
    json_serializer.serialize([material], ensure_ascii=False, stream=response)
    return response

def solid(request):
    pk = int(request.GET["pk"])
    material = Material.objects.get(pk=pk)
    material.solid = not material.solid
    material.save()

    response = http.HttpResponse(content_type="application/json")
    response.write("[]")
    return response
