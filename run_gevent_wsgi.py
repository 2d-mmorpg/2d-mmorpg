#!/usr/bin/env python
from gevent import monkey; monkey.patch_all()
from gevent.wsgi import WSGIServer

from game.wsgi import application

server = WSGIServer(("127.0.0.1", 8001), application)
server.serve_forever()
